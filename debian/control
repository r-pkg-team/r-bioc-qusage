Source: r-bioc-qusage
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-limma,
               r-bioc-biobase,
               r-cran-nlme,
               r-cran-emmeans,
               r-cran-fftw
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-qusage
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-qusage.git
Homepage: https://bioconductor.org/packages/qusage/
Rules-Requires-Root: no

Package: r-bioc-qusage
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: qusage: Quantitative Set Analysis for Gene Expression
 This package is an implementation the Quantitative Set
 Analysis for Gene Expression (QuSAGE) method described in
 (Yaari G. et al, Nucl Acids Res, 2013). This is a novel Gene
 Set Enrichment-type test, which is designed to provide a
 faster, more accurate, and easier to understand test for gene
 expression studies. qusage accounts for inter-gene correlations
 using the Variance Inflation Factor technique proposed by Wu et
 al. (Nucleic Acids Res, 2012). In addition, rather than simply
 evaluating the deviation from a null hypothesis with a single
 number (a P value), qusage quantifies gene set activity with a
 complete probability density function (PDF). From this PDF, P
 values and confidence intervals can be easily extracted.
 Preserving the PDF also allows for post-hoc analysis (e.g.,
 pair-wise comparisons of gene set activity) while maintaining
 statistical traceability. Finally, while qusage is compatible
 with individual gene statistics from existing methods (e.g.,
 LIMMA), a Welch-based method is implemented that is shown to
 improve specificity. For questions, contact Chris Bolen
 (cbolen1@gmail.com) or Steven Kleinstein
 (steven.kleinstein@yale.edu)
